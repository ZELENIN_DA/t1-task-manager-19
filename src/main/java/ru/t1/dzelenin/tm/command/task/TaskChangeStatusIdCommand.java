package ru.t1.dzelenin.tm.command.task;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusId(id, status);
    }

    @Override
    public String getName() {
        return "task-change-status-by-id";
    }

    @Override
    public String getDescription() {
        return "Change task status by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
