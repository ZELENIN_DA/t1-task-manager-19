package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusId(id, status);
    }

    @Override
    public String getName() {
        return "project-start-id";
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
